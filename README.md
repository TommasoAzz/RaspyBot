# RaspyBot
A telegram bot to manage a Raspberry Pi

# How to setup
Follow this steps to setup RaspyBot.

### 1. Create a new bot on telegram
Go to the [Telegram official guide](https://core.telegram.org/bots#3-how-do-i-create-a-bot) to learn how to create a new bot.

### 2. Clone this repository on your raspberry
```
git clone https://github.com/jjocram/RaspyBot
```

### 3. Create the config file
Create in the cloned directory the file `config.toml` and inside it write
```
BOT_TOKEN = your_bot_token
```
Replace `your_bot_token` with the token generated following the guide followed at Step 1.

### 4. Create the access list
Add one or more Telegram usernames in the file `accepted_usernames.txt` (after the explanatory text).
Only those users will be allowed to chat with your instance of RaspyBot.

### 5. Create a new systemd serivce.
Create new file: 
```
/lib/systemd/system/raspy_bot.service
```

**Remeber to:** 
  - change `YOUR_USER` with your username on the Raspberry Pi;
  - change `PATH_TO_RASPYBOT_DIR` with the absolute path to reach the directory created when your cloned this repository;
  - install all the dependencies with:
```
pip3 install -r requirements.txt
```

```
[Unit]
Description=Run RaspyBot on startup
Wants=network.target
After=network.target
After=multi-user.target

[Service]
WorkingDirectory=/home/pi/
User=YOUR_USER
ExecStart=/usr/bin/python3 PATH_TO_RASPYBOT_DIR/bot.py
Restart=always

[Install]
WantedBy=multi-user.target
```

### 4. Enable the service to run at startup
```
sudo systemctl enable raspy_bot.service
```

### 5. Start your bot
```
sudo systemctl start raspy_bot.service
```
