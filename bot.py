from re import sub
from typing import Dict, List
import requests
import logging
from telegram import Update
import telegram
from telegram.ext import Updater, CommandHandler, Filters, CallbackContext, MessageHandler

from os import popen
from os.path import exists as exists_file
import subprocess
import toml

"""
Logging stuff
"""
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

def get_help(update: Update, context: CallbackContext):
    update.message.reply_text(
        "RaspyBot\n" +
        "Available commands:\n" +
        "- <b>/get_ip</b> or <b>/ip</b> : returns the external and internal IP of the Raspberry Pi.\n" +
        "- <b>/get_temp</b>, <b>/temp</b> or <b>/temperature</b> : returns the device temperature.\n" + 
        "- <b>/uptime</b> : calls the command <pre>uptime</pre> and returns its output, formatted.\n" +
        "- <b>/update</b> or <b>/upd8</b> : calls <pre>sudo apt update</pre> and <pre>sudo apt upgrade -y</pre>.\n" +
        "- <b>/help</b> : displays this help message.",
        parse_mode=telegram.ParseMode.HTML
    )
    

def execute_request(update: Update, context: CallbackContext):
    update.message.reply_text("Type in the command you want to execute on the Pi.")
    users_commands.append(update.effective_user)


def execute_perform(update: Update, context: CallbackContext):
    if update.effective_user in users_commands:
        users_commands.remove(update.effective_user)
        if(len(update.message.text.split(";")) > 1):
            # Many commands
            commands = update.message.text.split(";")
            for i in range(len(commands)-1):
                if commands[i].strip() in ["[nowait]", "[wait]"]:
                    continue
                proc = subprocess.Popen(commands[i].strip().split(" "), shell=True)
                if commands[i+1].strip() == "[nowait]":
                    continue
                proc.wait()
            if commands[i].strip() not in ["[nowait]", "[wait]"]:
                subprocess.Popen(commands[i].strip().split(" "), shell=True).wait()
        else:
            subprocess.Popen(update.message.text.strip().split(" "), shell=True)
        logger.info("Inviato")

def update_system(update: Update, context: CallbackContext):
    update.message.reply_text("Starting the update and upgrade processes. You will receive another message when the process ends.")

    update_process = subprocess.run(["sudo", "apt", "update"])
    upgrade_process = subprocess.run(["sudo", "apt", "upgrade", "-y"])

    if update_process.returncode == 0 and upgrade_process.returncode == 0:
        update.message.reply_text("The process ended successfully!")
    else:
        update.message.reply_text(f"The update process returned status code: {update_process.returncode}\nThe upgrade process returned status code: {upgrade_process.returncode}")


def get_ip(update: Update, context: CallbackContext):
    external_ip = requests.get('http://ifconfig.me').text
    internal_ip = popen("hostname -I").read().split(" ")[0]
  
    update.message.reply_text(f"<b>External IP</b>: {external_ip}\n<b>Internal IP</b>: {internal_ip}", parse_mode=telegram.ParseMode.HTML)


def get_temp(update: Update, context: CallbackContext):
    temp = popen("vcgencmd measure_temp").read().replace("temp=", "").replace("\'", "°")

    update.message.reply_text(f"<b>Temperature</b>: {temp}", parse_mode=telegram.ParseMode.HTML)


def get_uptime(update: Update, context: CallbackContext):
    uptime_result = popen("uptime").read()

    """
    Example of uptime_result: 20:43:26 up  5:40,  2 users,  load average: 0,25, 0,33, 0,26
    0) 20:43:26 <<-- is the current time
    1) up <<-- descriptive string
    2)  5:40 <<-- total up time
    3)  2 users <<-- total number of logged in users
    4)  load average: 0,25, 0,33, 0,26 <<-- load average in the past 1, 5, 15 minutes
    """

    current_device_time = uptime_result.split(" up")[0]
    uptime = uptime_result.split("up")[1].strip().split(",")[0]
    users = uptime_result.split(" users")[0].split(",")[1].strip()
    load_average = uptime_result.split("load average: ")[1]

    update.message.reply_text(
        f"<b>Current time</b>: {current_device_time}\n" +
        f"<b>Uptime</b>: {uptime}\n" +
        f"<b>Users</b>: {users}\n" +
        f"<b>Load average in the past 1, 5, 15 minutes</b>: {load_average}",
        parse_mode=telegram.ParseMode.HTML
    )


users_commands: List[str] = list()

def main():
    with open("config.toml") as config_file:
        config = toml.loads(config_file.read())
        TOKEN = config["BOT_TOKEN"]
    
    # Loading the accepted usernames
    if exists_file('./accepted_usernames.txt'):
        with open('./accepted_usernames.txt', mode='r',encoding="utf8") as un_list:
            USERNAMES = un_list.read().split('\n')
            # Skipping the first 2 rows since there are explanations.
            for un in USERNAMES[2:]:
                if not un.startswith('@'):
                    un = '@' + un
    else:
        USERNAMES = list()

    logger.info(f"Telegram users allowed to use the bot: {USERNAMES[2:]}")

    # Create the user filter for the handlers
    user_filter = Filters.user(username=USERNAMES)

    # Create the Updater and pass it your bot's token.
    updater = Updater(TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(CommandHandler(['get_ip', 'ip'], get_ip, user_filter))
    dp.add_handler(CommandHandler(['get_temp', 'temp', 'temperature'], get_temp, user_filter))
    dp.add_handler(CommandHandler('uptime', get_uptime, user_filter))
    dp.add_handler(CommandHandler(['update', 'upd8'], update_system, user_filter))
    dp.add_handler(CommandHandler('execute', execute_request, user_filter))
    dp.add_handler(CommandHandler('help', get_help, user_filter))

    dp.add_handler(MessageHandler(Filters.text & ~Filters.command & user_filter, execute_perform))
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
